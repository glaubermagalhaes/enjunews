var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var fileinclude = require('gulp-file-include');
var csso        = require('gulp-csso');
var concat      = require('gulp-concat');
var prefix      = require('gulp-autoprefixer');
var rename      = require("gulp-rename");
var prettify    = require('gulp-prettify');
var sourcemaps  = require('gulp-sourcemaps');
var imagemin    = require('gulp-imagemin');
var uglify      = require('gulp-uglify');

var watch = require('./semantic/tasks/watch');
var build = require('./semantic/tasks/build');
gulp.task('watch ui', watch);
gulp.task('build ui', build);

// Static Server + watching scss/html files
gulp.task('watch', ['sass', 'css', 'js'], function() {

    browserSync.init({
        proxy: "http://localhost:8000"
    });

    gulp.watch("assets/scss/**/*.scss", ['sass']);
    gulp.watch("assets/css/*.css", ['css']);
    gulp.watch("assets/js/*.js", ['js_build']);
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    gulp.src("./assets/scss/*.scss")
        .pipe(sass())
        .pipe(prefix("last 1 version", "> 1%", "ie 8", "ie 7"))
        .pipe(gulp.dest("./css"))
        .pipe(browserSync.stream());
});

gulp.task('sass_build', function() {
    gulp.src('./assets/scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(prefix("last 1 version", "> 1%", "ie 8", "ie 7"))
        .pipe(csso())
        .pipe(rename(function (path) {
            path.basename += ".min"
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./css/'));
});

// Move css file and create sourcemap file
gulp.task('css', function() {
    gulp.src('./assets/css/*.css')
        .pipe(gulp.dest('./css/'))
        .pipe(browserSync.stream());
});

gulp.task('css_build', function() {
    gulp.src('./assets/css/*.css')
        .pipe(sourcemaps.init())
        .pipe(csso())
        .pipe(rename(function (path) {
            path.basename += ".min"
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./css/'));
});

// Minification JS and create sourcemap file
gulp.task('js', function() {
    gulp.src('./assets/js/*.js')
        .pipe(gulp.dest('./js'))
        .pipe(browserSync.stream());
});

gulp.task('js_build', function() {
    gulp.src([
            './assets/js/browser.js',
            './assets/js/react.min.js',
            './assets/js/react-dom.min.js',
            './semantic/dist/semantic.js',
        ])
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(rename(function (path) {
            path.basename += ".min"
        }))
        .pipe(gulp.dest('./js'));
});

gulp.task('default', ['watch']);

gulp.task('build', ['sass_build', 'css_build', 'js_build']);
